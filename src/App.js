import React from 'react'

function CustomButton() {
  return (
    <button>
      Custom Button
    </button>
  );
}


function App() {
  return (
    <div>
      <div>App</div>
      
      <CustomButton />
    </div>
  )
}

export default App